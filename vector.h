#pragma once

class vector
{
private:
    double x, y, z;
public:
    vector() : x(0), y(0), z(0) {}
    vector(double t) : x(t), y(t), z(t) {}
    vector(double _x, double _y, double _z): x(_x), y(_y), z(_z) {}

    double GetX() const;
    double GetY() const;
    double GetZ() const;

    double GetLenth() const;
};

