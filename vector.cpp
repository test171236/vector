#include <cmath>

#include "vector.h"

double vector::GetX() const
{
    return x;
}

double vector::GetY() const
{
    return y;
}

double vector::GetZ() const
{
    return z;
}

double vector::GetLenth() const
{
    return sqrt(x*x + y*y + z*z);
}
