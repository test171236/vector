#include <iostream>

#include "vector.h"

void PrintVector(const vector& v)
{
    std::cout << "(" << v.GetX() << ", " << v.GetY() << ", " << v.GetZ() << ")\n";
    std::cout << "Length: " << v.GetLenth() << std::endl;
}

int main()
{
    vector v1;
    PrintVector(v1);

    vector v2(2);
    PrintVector(v2);

    vector v3(1, 2, 3);
    PrintVector(v3);

    return 0;
}